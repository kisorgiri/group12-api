// schema defination for users collection

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    name: {
        type: String,
        required: true,
        lowercase: true
    },
    phoneNumber: Number,
    address: String,
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        unique: true,
        sparse: true,
    },
    gender: {
        type: String,
        enum: ['male', 'female', 'others'],
    },
    dob: {
        type: Date
    },
    status: {
        type: String,
        enum: ['online', 'offline'],
        default: 'offline'
    },
    roles: {
        type: Number,
        enum: [1, 2, 3], // 1 for admin, 2 normal user, 3 visitors
        default: 2
    },
    updatedBy: String,
    activeStatus: {
        type: Boolean,
        default: false
    },
    passwordResetTokenExpiry: Date,
    passwordResetToken: String
}, {
        timestamps: true
    });
var UserModel = mongoose.model('user', UserSchema);
module.exports = UserModel;