var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ratingSchema = new Schema({
    point: Number,
    productRef: {
        type: Schema.Types.ObjectId,
        ref: 'product'
    },
    userRef: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
}, {
        timestamps: true
    });


module.exports = mongoose.model('rating', ratingSchema);