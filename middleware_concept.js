//
// what comes out // router
/// third party middleware
/// middleware
// middleware is a function that has access to http request object, http response object
// and next middleware function refrence
// middleware are placed in between http request response cycle;
// the order of middleware function matters
// middleware are place in a config block called app.use();

// trick to detect middleware function
//  function that must have at least two argument which will be 1st as request and 2nd as response
//  any time middleware function are kept in inside app.use() block

// middleware have powers to access http request object http response object and modify them
// types of middleware
//  5types of middleware
// application level middleware
// routing level middleware
// error handling middleware
// thirdparty middleware
// inbuilt middleware
// application level middleware