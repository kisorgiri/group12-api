module.exports = function (req, res, next) {
    if (req.headers.token) {
        return next();
    } else {
        next({
            message: 'you dont have token',
            status: 400
        });
    }
}