var jwt = require('jsonwebtoken');
var config = require('./../config');

var UserModel = require('./../models/user.model');

module.exports = function (req, res, next) {
    var token;
    if (req.headers['x-access-token']) {
        token = req.headers['x-access-token'];
    }
    if (req.headers['authorization']) {
        token = req.headers['authorization'];
    }
    if (req.headers['token']) {
        token = req.headers['token'];
    }
    if (req.query.token) {
        token = req.query.token;
    }
    if (token) {

        jwt.verify(token, config.jwt_secet, function (err, verified) {
            if (err) {
                return next(err);
            }
            console.log('verified >>', verified);

            UserModel.findOne({ _id: verified.id }, function (err, user) {
                if (err) {
                    return next(err);
                }
                if (user) {
                    req.loggedInUser = user;
                    return next();
                }
                else {
                    next({
                        message: 'User removed from system'
                    })
                }

            })
        })

    } else {
        next({
            message: 'Token not provided'
        });
    }
}

// revision
// jsonwebtoken
// token based authentication
// token >> generate using secret key
// token vrification using same secret
//  after verification add user(loggedUser) property in req.object
// posiible failure
// if user is removed from system token will still work
// if user update theri info the the info in token will be not be updated
// solution found and implemented