var config = require('./config');
var users = [];
module.exports = function (app) {
    var socket = require('socket.io');
    var io = socket(app.listen(config.socketPort))
    console.log('socket server listening at port 9091')
    io.on('connection', function (client) {
        console.log('client connected to server');
        var socketId = client.id;
        client.on('new-user', function (data) {
            users.push({
                id: socketId,
                name: data
            });
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })

        client.on('new-msg', function (data) {
            //  intresting part
            console.log('msg at BE >>', data);
            client.emit("reply-msg-own", data); // this will emit event to requested client only
            client.broadcast.to(data.receiverId).emit('reply-msg', data); // this will emit for every conected client execpt own 
        })
        client.on('typing',function(data){
            client.broadcast.to(data.receiverId).emit('is-typing');
        })
        client.on('typing-stop',function(data){
            client.broadcast.to(data.receiverId).emit('is-typing-stop');
            
        })

        client.on('disconnect', function (data) {
            console.log('disconnected data >>', data);
            users.forEach(function (item, i) {
                if (item.id == socketId) {
                    users.splice(i, 1);
                }
            });
            client.broadcast.emit('users', users);
        })

    });
}

