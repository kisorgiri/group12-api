var express = require('express');
var router = express.Router();
var UserModel = require('./../models/user.model');
var passwordHash = require('password-hash');
var mapUser = require('./../helpers/map_user_req');
var config = require('./../config');
var generateToken = require('./../helpers/create-token');
var randomString = require('randomstring');
var sender = require('./../config/nodemailer.config');

function prepareMail(details) {
    var msgBody = {
        from: '"Group-12 IMS 👻" <noreply@group12.com>', // sender address
        to: "rumus.3353@gmail.com,suyogpradhan711@gmail.com,amitm700@gmail.com,thapaojaswee01@gmail.com,aasthabasnet99@gmail.com," + details.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `<p>Hello <b>${details.name}</b>,</p>
            <p> We noticed that you are having trouble logging into our system please use the link below to reset password.</p>
            <p> <a href="${details.link}" target="_blank">click here to reset password </a> </p>
            <p> Regards,</p>
            <p>Group 12 IMS</p>`,

    }
    return msgBody;
}

module.exports = function (app, arg2) {
    router.get('/login', function (req, res, next) {
        console.log('empty route inside auth');
        console.log('req.query  >>>>', req.query);
        res.render('login.pug', {
            title: 'hello',
            message: req.query.message || '',
        });
    });

    router.post('/login', function (req, res, next) {
        UserModel.findOne({
            username: req.body.username,
        })
            .exec(function (err, user) {
                if (err) {
                    return next(err);
                }
                if (user) {
                    // if (!user.activeStatus) {
                    //     return next({
                    //         message: 'Account is disabled please contact your administrator'
                    //     })
                    // }
                    var isMatch = passwordHash.verify(req.body.password, user.password);
                    if (isMatch) {
                        // generate token here
                        var token = generateToken.createToken({
                            id: user._id,
                            name: user.username
                        }, config.jwt_secet);
                        res.json({
                            user: user,
                            token: token
                        });
                    } else {
                        next({
                            message: 'invalid login credentials password'
                        })
                    }
                    // 
                } else {
                    next({
                        message: "UserName invalid"
                    })
                }
            });
    });

    router.route('/register')
        .get(function (req, res, next) {
            console.log('here at auth route ');
            // res.sendStatus(200);
            res.render('register.pug');
        })
        .post(function (req, res, next) {
            console.log('data here >>', req.body);
            // prepare object to be saved
            var newUser = new UserModel({});
            var newMappedUser = mapUser(newUser, req.body);
            newMappedUser.save(function (err, saved) {
                if (err) {
                    return next(err);
                }
                res.json(saved);
            })

        })
        .put(function (req, res, next) {

        })
        .delete(function (req, res, next) {

        });

    router.route('/forgot-password')
        .post(function (req, res, next) {
            var email = req.body.email;
            console.log('req.headers >>', req.headers);
            UserModel.findOne({
                email: email
            })
                .exec(function (err, user) {
                    if (err) {
                        return next(err);
                    }
                    if (user) {

                        // send email

                        var token = randomString.generate(25);
                        var expiryTime = new Date().getTime() + 1000 *
                            60 * 60 * 24;
                        var mailBody = prepareMail({
                            name: user.username,
                            email: user.email,
                            link: req.headers.origin + '/auth/reset-password/' + token
                        });

                        user.passwordResetToken = token;
                        user.passwordResetTokenExpiry = new Date(expiryTime);
                        user.save(function (err, done) {
                            if (err) {
                                return next(err);
                            }
                            sender.sendMail(mailBody, function (err, done) {
                                if (err) {
                                    return next(err);
                                }
                                res.json(done);
                            });
                        });

                    } else {
                        next({
                            message: "Email not registered yet"
                        });
                    }
                })
        })

    router.route('/reset-password/:token')
        .post(function (req, res, next) {
            var token = req.params.token;
            UserModel.findOne({
                passwordResetToken: token,
                passwordResetTokenExpiry: {
                    $gte: Date.now()
                }
            })
                .exec(function (err, user) {
                    if (err) {
                        return next(err);
                    }
                    if (user) {
                        // proceed with password change
                        user.password = passwordHash.generate(req.body.password);
                        user.passwordResetToken = null;
                        user.passwordResetTokenExpiry = null;
                        user.save(function (err, done) {
                            if (err) {
                                return next(err);
                            }
                            res.json(done);
                        })


                    } else {
                        next({
                            message: 'Token Expired'
                        })
                    }
                })
        })


    return router;
}