var express = require('express');
var router = express.Router();
var UserModel = require('./../models/user.model');
var passwordHash = require('password-hash');

var config = require('./../config');
var mapUser = require('./../helpers/map_user_req');

router.route('/')
    .get(function(req, res, next) {

        // res.end('hello from get from user');
        // UserModel.find({})
        //     .then(function (data) {
        //         res.json(data)
        //     })
        //     .catch(function (err) {
        //         next(err);
        //     })
        UserModel.find({})
            .sort({
                _id: -1
            })
            .exec(function(err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
    })
    .post(function(req, res, next) {

    });

router.route('/change-password')
    .get(function(req, res, next) {
        res.end('from change password');
    })
    .post(function(req, res, next) {

    })

.put(function(req, res, next) {

})

.delete(function(req, res, next) {

});

router.route('/:id')
    .get(function(req, res, next) {
        console.log('req.params >>', req.params);
        console.log('req.loggedInuser >>>', req.loggedInUser);
        // UserModel.findOne({
        //     _id: req.params.id
        // })
        //     .exec(function (err, user) {
        //         if (err) {
        //             return next(err);
        //         }
        //         res.json(user);

        //     })
        UserModel.findById(req.params.id, function(err, user) {
            if (err) {
                return next(err);
            }
            res.json(user);
        })
    })
    .post(function(req, res, next) {

    })

.put(function(req, res, next) {
    console.log('req.loggedInuser >>>', req.loggedInUser);

    UserModel.findById(req.params.id, function(err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            // if user exist update
            var updatedUser = mapUser(user, req.body);
            updatedUser.updatedBy = req.loggedInUser.name;
            console.log('updated user >>>',updatedUser);
            updatedUser.save(function(err, saved) {
                if (err) {
                    return next(err);
                }
                res.json(saved);
            })
        } else {
            next({
                message: "user not found"
            })
        }
    })
})

.delete(function(req, res, next) {
    UserModel.findById(req.params.id, function(err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            user.remove(function(err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        } else {
            next({
                message: "user not found"
            })
        }
    })
});





module.exports = router;