var router = require('express').Router();
var ProductRoute = require('./../components/products/routes/product.routes');
var authentication = require('./../middlewares/authenticate');
var notificationRoute = require('./../components/notification/controller/notification.route');

//load componets here

router.use('/product', authentication, ProductRoute)
router.use('/notification', notificationRoute);
router.use('/notification', ProductRoute)
router.use('/test', ProductRoute)

module.exports = router;