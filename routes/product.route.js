var express = require('express');
var router = express.Router();
var multer = require('multer');
var fs = require('fs');
var path = require('path');
var ProductModel = require('./../components/products/models/product.model');


// var upload = multer({
//     dest: './files/uploads/'
// })

var store = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    },
    destination: function (req, file, cb) {
        cb(null, './files/uploads/')
    }
});

function filter(req, file, cb) {
    var mimetype = file.mimetype;
    var image = mimetype.split('/')[0];
    if (image == 'image') {
        cb(null, true);
    } else {
        req.fileError = 'sdklfh';
        cb(null, false);
    }
}
var upload = multer({
    storage: store,
    // fileFilter: filter
});


function map_product_req(product, productDetails) {
    if (productDetails.name)
        product.name = productDetails.name;
    if (productDetails.brand)
        product.brand = productDetails.brand;
    if (productDetails.category)
        product.category = productDetails.category;
    if (productDetails.description)
        product.description = productDetails.category;
    if (productDetails.quantity)
        product.quantity = productDetails.quantity;
    if (productDetails.colors)
        product.colors = productDetails.colors.split(',');
    if (productDetails.warrenty) {
        product.warrenty = {
            status: productDetails.warrenty,
            warrentyDescription: productDetails.warrentyDescription,
            warrentyPeriod: productDetails.warrentyPeriod
        }
    }
    if (productDetails.price)
        product.price = productDetails.price;
    if (productDetails.modelNo)
        product.modelNo = productDetails.modelNo;
    if (productDetails.tags)
        product.tags = productDetails.tags.split(',');
    if (productDetails.status)
        product.status = productDetails.status;
    if (productDetails.image)
        product.image = productDetails.image;

    return product;

}

module.exports = function (ev) {

    router.route('/')
        .get(function (req, res, next) {
            var condition = {};
            // business login
            if (req.loggedInUser.role == 2) {
                condition.user = req.loggedInUser._id;
            }
            ProductModel.find(condition)
                .populate('user', {
                    username: 0,
                })
                .exec(function (err, products) {
                    if (err) {
                        return next(err);
                    }
                    ev.emit('products',products);
                    res.json(products);
                });
        })
        .post(upload.single('images'), function (req, res, next) {
            console.log('req.body ?>>>', req.body);
            console.log('req.file >>>', req.file);
            if (req.fileError) {
                return next({
                    message: 'invalid file format'
                })
            }
            ///////#### if file filter is not used#######################
            // if (req.file) {
            //     var mimetype = req.file.mimetype;
            //     var image = mimetype.split('/')[0];
            // }      
            // if (image !== 'image') {
            //     fs.unlink(path.join(process.cwd(), 'files/uploads/' + req.file.filename), function (err, done) {
            //         if (err) {
            //             console.log('error');
            //         } else {
            //             console.log('removed');
            //         }

            //     })
            //     return next({
            //         message: "invalid file format"
            //     });
            // }

            ///###################file filter
            var newProduct = new ProductModel({});
            var newMappedProduct = map_product_req(newProduct, req.body);
            if (req.file) {
                newMappedProduct.image = req.file.filename;
            }

            // adding user from logged inuser 
            newMappedProduct.user = req.loggedInUser._id;
            console.log('save proudct now <>>>>>', newMappedProduct);
            newMappedProduct.save(function (err, saved) {
                if (err) {
                    return next(err);
                }
                res.json(saved);
            })
        });

    router.route('/search')
        .get(function (req, res, next) {
            var condition = {};
            var searchCondition = map_product_req(condition, req.query)
            ProductModel.find(searchCondition, function (err, results) {
                if (err) {
                    return next(err);
                }
                res.json(results);
            })
        })
        .post(function (req, res, next) {
            console.log('req.body >>', req.body);
            var condition = {};
            var searchCondition = map_product_req(condition, req.body);

            if (req.body.minPrice) {
                searchCondition.price = {
                    $gte: req.body.minPrice
                }
            }
            if (req.body.maxPrice) {
                searchCondition.price = {
                    $lte: req.body.maxPrice
                }
            }

            if (req.body.minPrice && req.body.maxPrice) {
                searchCondition.price = {
                    $lte: req.body.maxPrice,
                    $gte: req.body.minPrice
                }
            }
            if (req.body.fromDate && req.body.toDate) {
                var fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0);
                var toDate = new Date(req.body.toDate).setHours(23, 59, 0, 0);
                searchCondition.createdAt = {
                    $gte: new Date(fromDate),
                    $lte: new Date(toDate)
                }
            }
            if (req.body.colors) {
                searchCondition.colors = {
                    $in: req.body.colors
                }
            }
            console.log('search condition >>', searchCondition);
            ProductModel.find(searchCondition, function (err, results) {
                if (err) {
                    return next(err);
                }
                res.json(results);
            })
        })

    router.route('/:id')
        .get(function (req, res, next) {
            ProductModel.findById(req.params.id, function (err, product) {
                if (err) {
                    return next(err)
                }
                if (product) {
                    res.json(product)
                }
                else {
                    next({
                        message: 'Product Not found'
                    })
                }
            })
        })
        .put(upload.array('images', 10), function (req, res, next) {
            console.log('req.body >>>', req.body);
            console.log('req.files >>>', req.files);
            // if (req.fileError) {
            //     return next({
            //         message: 'Invalid file format'
            //     })
            // }
            if (req.files) {
                var mimetype = req.files[0].mimetype;
                var image = mimetype.split('/')[0];
                if (image !== 'image') {
                    fs.unlink(path.join(process.cwd(), 'files/uploads/' + req.files[0].filename), function (err, done) {
                        if (err) {
                            console.log('err');
                        } else {
                            console.log('removed');
                        }
                    });
                    return next({
                        message: 'invalid file format'
                    })
                }

            }
            var id = req.params.id;
            ProductModel.findById(id, function (err, product) {
                if (err) {
                    return next(err);
                }
                if (product) {
                    var oldImage = product.image;
                    var updatedProduct = map_product_req(product, req.body);
                    if (req.body.ratings)
                        updatedProduct.ratings.push({
                            point: req.body.point,
                            message: req.body.ratingComments,
                            by: req.loggedInUser._id,
                            productRef: product._id,
                        });
                    if (req.files[0]) {
                        updatedProduct.image = req.files[0].filename;
                    }
                    updatedProduct.save(function (err, updated) {
                        if (err) {
                            return next(err);
                        }
                        if (req.files[0]) {
                            fs.unlink('./files/uploads/' + oldImage, function (err, done) {
                                if (err) {
                                    console.log('err');
                                } else {
                                    console.log('done');
                                }
                            });
                        }
                        res.json(updated)
                    })
                }
                else {
                    next({
                        message: 'product not found'
                    })
                }
            })
        })
        .delete(function (req, res, next) {
            ProductModel.findByIdAndRemove(req.params.id, function (err, removed) {
                if (err) {
                    return next(err);
                }
                res.json(removed);
            });
        });




    return router;
}