var passwordHash = require('password-hash');

module.exports = function(user, userDetails) {
    if (userDetails.name)
        user.name = userDetails.name;
    if (userDetails.username)
        user.username = userDetails.username;
    if (userDetails.password)
        user.password = passwordHash.generate(userDetails.password);
    if (userDetails.email)
        user.email = userDetails.email;
    if (userDetails.address)
        user.address = userDetails.address;
    if (userDetails.phoneNumber)
        user.phoneNumber = userDetails.phoneNumber;
    if (userDetails.gender)
        user.gender = userDetails.gender;
    if (userDetails.dob)
        user.dob = userDetails.dob;
    if (userDetails.role)
        user.roles = userDetails.role;
    if (userDetails.status)
        user.status = userDetails.status;
    user.activeStatus = userDetails.activeStatus ? true : false;

    return user;
}