var event = require('events');

var event1 = new event.EventEmitter();
var event2 = new event.EventEmitter();
var event3 = new event.EventEmitter();

// there two parts in event dirvern communication
// listerns 
// and emitter
// emit function for emitters and on method for listenrs

setTimeout(() => {

    console.log('ram event fired');
    event1.emit('ram',{test:'test'});
}, 2000);

event1.on('ram', function (data) {
    console.log('listener for ram event')
    console.log('data >>', data);
})  