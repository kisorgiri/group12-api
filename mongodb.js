// mongod /// command to initialize mongodb driver
// mongo is a command to access mongo shell where we execute command

// command

// show dbs // list all the database of the system
// use <db_name>
/// if(db_name exist){
//     select existing db
// }else{
//     create new db  and select created db
// }

// db command will show the selected db

// CRUD operation
//show collections
// insert data in collections
// db.<col_name>.insert({valid json});
// inserting array into collections
// db.<col_name>.insert(valid Array);
// db.<col_name>.insertMany(valid Array);

//fetching data from database
// db.<col_name>.find({query builder})
// db.<col_name>.find({}).pretty() // prettify content

//db.<col_name>.count() // return total number of documents inside collections

// db.<col_name>.find({}).sort({
// field(_id) : -1 // -1 is for decending // 
// })
// .limit(3);
    // .skip(2)
    // // projection
    // db.<col_name>.find({},{projection(name:1 // 1 for inclusion)})

// update
// db.<col_name>.update({},{$set:{name:'ram',address:'tinkune'}}) // only one document will be updated
// for multiple update
// add third objec tin update method
// db.<col_name>.update({_id:ObjectId('hex code)},{$set:{name:'ram',address:'tinkune'}},{mutli:true}) // only one document will be updated

// the first objec of update command is for query

// remove
// db.<col_name>.remove({_id:'some value'}) // remove all the documents

// droppiong collection
// db.<col_name>.drop();

// drop database 
// db.dropDatabase();

//##################BACKUP AND RESTORE###################

// BACKUP
/// bson structure and json & csv
// bson data
// command
// mongodump
// mongodump is a command to backup all the database in a default dump folder
// mongodump --db <db_name>  (selected database)
// mongodump --db<db_name> --out <destination_path_to_folder)

//restore
// mongorestore // this command will restore bson  backed up data  search for dump folder
//mongorestore path_to_destination_backup_db

// mongodump and mongorestore always came in pair

// JSON and CSV backup
// 1 JSON
// backup 
// command
// mongoexport
// mongoexport --db <db_name> --collection<col_name> --out <path_to_destination_folder>with .json extention file


//restore or import
// mongoimport
// mongoimport --db new-db --collection col_name path_to_file containing folder

// csv file
// mongoexport  
// mongoexport --db db_name --collection col_name --type=csv --files 'comma separeted field name ' --out destination_folder /filename.csv ;// this filed name will be exported in exist in dabatabase







