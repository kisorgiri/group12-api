var express = require('express');
var router = express.Router();
var productCtrl = require("./../controllers/product.ctrl");

router.route('/')
    .get(productCtrl.get)
    .post(productCtrl.post);

router.route('/search')
    .get(productCtrl.search)
    .post(productCtrl.search);

router.route('/:id')
    .get(productCtrl.getById)
    .put(productCtrl.put)
    .delete(productCtrl.remove);


module.exports = router;