var productQuery = require('./../queries/product.query');
/**
 * this is product fetching cotroller
 * @param {object} req 
 * @param {object} res 
 * @param {function} next 
 */
function get(req, res, next) {
    // prepare logic
    console.log('i should be here');
    var condition = {};
    if (req.loggedInUser.role !== 1) {
        condition.user = req.loggedInUser._id;
    }
    console.log('condition is >>', condition);
    productQuery.find(condition, function (err, response) {
        if (err) {
            return next(err);
        }
        res.status(200).json(response);
    });

}

function getById(req, res, next) {
    var id = req.params.id;
    var condition = {
        _id: id
    }
    productQuery.find(condition, function (err, response) {
        if (err) {
            return next(err);
        }
        res.status(200).json(response[0]);
    });
}

function post(req, res, next) {
    // logic // data preparation
    // TO DO file
    var data = req.body;
    data.user = req.loggedInUser._id;
    productQuery.insert(data)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function put(req, res, next) {
    // attach anything required in req.body
    productQuery.update(req.params.id, req.body)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function remove(req, res, next) {
    productQuery.remove(req.params.id)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}
function search(req, res, next) {

}



module.exports = {
    get, post, put, remove, search, getById
}