var ProductModel = require("./../models/product.model");
var mapProduct = require("./../helpers/product_req_map");

function insert(data) {
    return new Promise(function (resolve, reject) {
        var newProduct = new ProductModel({});
        mapProduct(newProduct, data);
        newProduct.save(function (err, saved) {
            if (err) {
                reject(err);
            }
            else {
                resolve(saved)
            }
        });
    });

}

function find(condition, cb) {
    ProductModel.find(condition)
        .exec(function (err, results) {
            if (err) {
                cb(err);
            } else {
                cb(null, results);
            }

        })
}
function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id)
            .exec(function (err, product) {
                if (err) {
                    reject(err);
                }
                if (product) {
                    mapProduct(product, data);
                    product.save(function (err, saved) {
                        if (err) {
                            reject(err)
                        }
                        else {
                            resolve(saved);
                        }
                    });
                }
                else {
                    reject({
                        message: 'Product not found'
                    })
                }
            })
    })

}
function remove(id) {
    return new Promise(function (resolve, reject) {
        ProductModel.findByIdAndRemove(id)
            .exec(function (err, done) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(done);
                }
            });
    });
}


module.exports = {
    insert, find, update, remove
}   