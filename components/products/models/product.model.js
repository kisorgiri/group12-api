var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ratingSchema = new Schema({
    point: Number,
    productRef: {
        type: Schema.Types.ObjectId,
        ref: 'product'
    },
    message: String,
    by: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
        timestamps: true
    });

var productSchema = new Schema({
    category: {
        type: String,
        required: true
    },
    brand: {
        type: String,
    },
    name: {
        type: String,
    },
    description: {
        type: String
    },
    colors: [String],
    quantity: Number,
    price: Number,
    status: {
        type: String,
        enum: ['availabel', 'out of stock'],
        default: 'availabel'
    },
    modelNo: String,
    ratings: [ratingSchema],
    warrenty: {
        status: {
            type: String
        },
        warrentyPeriod: String,
        warrentyDescription: String
    },
    image: String,
    tags: [String],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
        timestamps: true
    });

var ProductModule = mongoose.model('product', productSchema);
module.exports = ProductModule;