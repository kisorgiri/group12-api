function map_product_req(product, productDetails) {
    if (productDetails.name)
        product.name = productDetails.name;
    if (productDetails.brand)
        product.brand = productDetails.brand;
    if (productDetails.category)
        product.category = productDetails.category;
    if (productDetails.description)
        product.description = productDetails.category;
    if (productDetails.quantity)
        product.quantity = productDetails.quantity;
    if (productDetails.colors)
        product.colors = productDetails.colors.split(',');
    if (productDetails.warrenty) {
        product.warrenty = {
            status: productDetails.warrenty,
            warrentyDescription: productDetails.warrentyDescription,
            warrentyPeriod: productDetails.warrentyPeriod
        }
    }
    if (productDetails.price)
        product.price = productDetails.price;
    if (productDetails.modelNo)
        product.modelNo = productDetails.modelNo;
    if (productDetails.tags)
        product.tags = productDetails.tags.split(',');
    if (productDetails.status)
        product.status = productDetails.status;
    if (productDetails.image)
        product.image = productDetails.image;
    if (productDetails.user)
        product.user = productDetails.user;

    return product;

}


module.exports = map_product_req;
