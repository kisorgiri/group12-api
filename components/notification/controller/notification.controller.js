const notificationQuery = require('../query/notification.query');
const mongoose = require('mongoose');

exports.getNotification = (req, res) => {
	const cond = {};
	if (req.loggedInUser.role === 1)
		cond.client_id = req.loggedInUser.client_id;
	notificationQuery.getNotification(cond, req.query.page).then(function(response) {
			res.send(response);
		})
		.catch((err) => {
			res.status(400).send(err);
		});
};
exports.searchNotificaiton = (req, res) => {
	const cond = {};
	if (req.loggedInUser.role === 1)
		cond.client_id = mongoose.Types.ObjectId(req.loggedInUser.client_id);
	if (req.body.unseen)
		cond.seen = false;
	if (req.body.seen)
		cond.seen = true;
	if (req.body.client_id)
		cond.client_id = mongoose.Types.ObjectId(req.body.client_id);
	// console.log('cond', cond);
	notificationQuery.searchNotificaiton(cond).then(function(response) {
			res.send(response);
		})
		.catch((err) => {
			res.status(400).send(err);
		});
};

exports.markAllRead = (req, res) => {
	const cond = {};
	if (req.loggedInUser.role === 1)
		cond.client_id = mongoose.Types.ObjectId(req.loggedInUser.id);

	notificationQuery.markAllRead(cond).then(function(response) {
			res.send(response);
		})
		.catch((err) => {
			res.status(400).send(err);
		});
};
exports.updateNotification = (req, res) => {
	const id = req.params.id;
	notificationQuery.updateNotification(id, req.body).then(function(response) {
			res.send(response);
		})
		.catch((err) => {
			res.status(400).send(err);
		});
};