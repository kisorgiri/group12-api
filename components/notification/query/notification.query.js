const NotificationModel = require('../model/notificationModel');
const Map_Notification = (notificationDetails, notification) => {
  if (notificationDetails.device_id)
    notification.device_id = notificationDetails.device_id;
  if (notificationDetails.client_id)
    notification.client_id = notificationDetails.client_id;
  if (notificationDetails.message)
    notification.message = notificationDetails.message;
  if (notificationDetails.seen)
    notification.seen = notificationDetails.seen;
  if (notificationDetails.type)
    notification.type = notificationDetails.type;
  if (notificationDetails.link)
    notification.link = notificationDetails.link;
  return notification;
};

exports.notify = (notificationDetails) => {
  return new Promise(function (resolve, reject) {
    const notification = new NotificationModel();
    const newNotification = Map_Notification(notificationDetails, notification);
    newNotification.save((err) => {
      if (err) {
        reject(err);
      }
      resolve({
        message: "Notification inserted successfully"
      });
    });
  });
};

exports.notify = (notificationDetails) => {
  return new Promise(function (resolve, reject) {
    const notification = new NotificationModel();
    const newNotification = Map_Notification(notificationDetails, notification);
    newNotification.save((err) => {
      if (err) {
        reject(err);
      }
      resolve({
        message: "Notification inserted successfully"
      });
    });
  });
};
exports.getNotification = (condition, page) => {
  // console.log(condition);
  return new Promise(function (resolve, reject) {
    const currentPage = page || 0;
    const perPage = 8;
    const skip = perPage * (currentPage - 1);

    NotificationModel.find(condition)
      .populate('device_id')
      .populate('client_id')
      .sort({
        _id: -1
      })
      .limit(perPage)
      .skip(skip)
      .exec(function (err, response) {
        if (err) {
          reject(err);
        }
        resolve(response);
      });
  });
};
exports.searchNotificaiton = (condition) => {
  return new Promise(function (resolve, reject) {
    NotificationModel.aggregate([{
      "$match": condition
    }, {
      $sort: {
        _id: -1
      }
    }, {
      $lookup: {
        from: "devices",
        localField: "device_id",
        foreignField: "_id",
        as: "device"
      }
    }], function (err, response) {
      if (err) {
        reject(err);
      }
      resolve(response);
    });
  });
};
exports.markAllRead = (cond) => {
  return new Promise(function (resolve, reject) {
    NotificationModel.update(cond, {
        "$set": {
          "seen": true
        }
      }, {
        upsert: false,
        multi: true
      },

      function (err, data) {
        if (err) {
          reject(err);
        }
        console.log('daata', data);
        resolve(data);
      });
  });
};
exports.updateNotification = (id, data) => {
  return new Promise(function (resolve, reject) {
    NotificationModel.findById(id, function (err, Notify) {
      if (err) {
        reject(err);
      }
      if (!Notify) {
        reject({
          message: "error fetching notificaiton"
        });
      } else {
        const updateNotification = Map_Notification(data, Notify);
        updateNotification.save((err) => {
          if (err) {
            reject(err);
          }
          resolve({
            message: "notification updated"
          });
        });
      }
    });
  });
};
