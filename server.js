var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var app = express();// now this app variable will hold entire express framework
var config = require('./config/index');
var cors = require('cors');

//load thirdparty middleware
var morgan = require('morgan');

var events = require('events');
var event1 = new events.EventEmitter();

event1.on('products', (data) => {
    console.log('data received in new-product event>>', data);
});

// socket stuff
require('./socket')(app);
// load routing level middleware
var authRoute = require('./routes/auth.route')(app, config)
var userRoute = require('./routes/user.route');
var productRoute = require('./routes/product.route')(event1);
var apiRoute = require('./routes/api.routes');
require('./config/db.config');


// load middlewares
var checkToken = require('./middlewares/checkToken');
var verifyToken = require('./middlewares/verifyToken');
var authenticate = require('./middlewares/authenticate');
// use thirdparty middleware

//view engine setup
var pug = require('pug');
app.set('view-engine', pug);
app.set('views', 'templates');

app.use(morgan('dev'));///
app.use(cors());

// inbuilt middleware
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/file', express.static(path.join(__dirname, 'files'))); //
app.get('/', function (req, res, next) {
    res.end('response from server')
});

// routing level middleware
app.get('/check', function (req, res) {
    res.send('everything ok');
})
app.use('/api', apiRoute);
app.use('/user', authenticate, userRoute);
app.use('/rider', checkToken, verifyToken, userRoute);
app.use('/product', authenticate, productRoute);
app.use('/notification', userRoute);
app.use('/auth', authRoute);
// evey middleware that is a express router is a routing level middleware


app.use(function (req, res, next) {
    console.log('i am another middleware function');
    res.status(404)
    // res.end('path not registered');
    next({
        message: 'No where to go',
        status: 404
    })
});


// this middleware will not prompt in http request response cycle
// this.middlewarewill always take 4 argument
// 1st arugument will work as place holder for errors
// /this is error handling middleware
// this middleware will come into action wheneer next with argument is called
// error will take anything that is passed in next
app.use(function (error, req, res, next) {
    console.log('i am errro handling a middleware', error);
    // res.end('from another middleware');//
    res.status(error.status || 400).json({
        msg: error.message || error,
        status: error.status || 400,
    })
});


app.listen(process.env.PORT || 8080, function (err, done) {
    if (err) {
        console.log('error listening');
    }
    else {
        console.log('server listening at port ' + config.port);
        console.log('press CTRL +C to exit ');
    }
});



